// console.log("Hello World!")

// if statement

/*
	executes a statement if a specified condition is true 
	- can stand alom without  the else statement

	Syntax:
		if(condition){
			statement/code block
		}
*/

let numA = -1;

if (numA < 0){
	console.log("Hello!");
};

console.log(numA < 0);

if (numA > 0){
	console.log("This statement will not be printed");
};

let city = "New York";

if(city == "New York"){
	console.log("Welcome to New York City!");

};


let numB = 1;

if (numA < 0){
	console.log("Hello!");
} else if (numB > 0) {
	console.log("World");
};


city = "Tokyo";


if(city === "New York"){
	console.log("Welcome to New York City!")
} else if (city==="Tokyo"){
	console.log("Welcome to Tokyo!")
};

if (numA > 0){
	console.log("Hello");
} else if (numB === 0){
	console.log("World");
} else{
	console.log("Again");
}


// Another Example
// let age = prompt("Enter your age: ");
// if (age <= 18){
// 	console.log("Not allowed to drink")
// } else{
// 	console.log("Matanda ka na, shot na!")
// }

/*
	Mini Activity


*/

function checkHeight(height){
	
	if (height < 150){
		console.log("Did not pass the minimum height requirement")
	}else{
		console.log("Passed the minimum height requirement")
	};
};

checkHeight(142);


let message = "No message";
console.log(message)

function determineTyphoonIntensity(windSpeed){

	if (windSpeed < 30){
		return "Not a Typhoon yet"
	} else if (windSpeed <= 61){
		return "tropical depression detected"
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return "Tropical storm detected"
	} else if (windSpeed >= 89 && windSpeed <= 117){
		return "Severe tropical storm detected"
	} else{
		return("Typhoon detected")
	}
}

message = determineTyphoonIntensity(69);
console.log(message)


if (message == "Tropical storm detected"){
	console.warn(message)
}


// Truthy and Falsy values
/*
	-in Javascript, a truth value is a value that is considered true when encountered in a boolean context
	-Values are considered true unless defined otherwise
	Falsy values/exceptions for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN

*/
// Truthy Examples

if(true){
	console.log("Truthy");
};

if(1){
	console.log("Truthy!");
}

if([]){
	console.log("Truthy");
}

// Falsy Examples
if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

if(undefined){
	console.log("Falsy");
}


// Conditional (Ternary) Operator
/*
	The Ternary Operator  takes in three operands
	1. condition
	2. expression to execute if the condition is falsy

	Syntax:
		(condition) ? ifTrue: ifFalse
*/

// Single Statement exectuion
let ternaryResult = (1 < 18) ? true: false;
console.log("Result of Ternary Operator: " + ternaryResult)


let name;

function isLegalAge(){
	name = "John";
	retur "You are of the legal age limit";
}

function isUnderAge(){
	nane = "Jane"

	return "You are inder the age limit"


}
let age = parseInt(productName("what is your age?"));
console.log(age);
let isLegalAge(): 


// Switch Statement

/*
	Syntax (expression){
		case value:
			statement
			break
		default:
			statement
	}
*/